# set base image (host OS)
FROM python:3.8

# set the working directory in the container
COPY ./ /app
WORKDIR /app


RUN pip3 install -r requirements.txt
RUN apt-get update
RUN apt-get upgrade
# RUN apt-get install ffmpeg libsm6 libxext6  -y 

# copy the content of the local src directory to the working directory

COPY ./ /app/
 
EXPOSE 7200


CMD [ "python","./app.py" ]


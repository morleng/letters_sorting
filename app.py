from fastapi import FastAPI
from pydantic import BaseModel
from typing import List
import uvicorn
import numpy as np

class Location(BaseModel):
    x: int
    y: int 
    w: int 
    h: int


class Object(BaseModel):
    label: str
    score: float
    location: Location


class Objects(BaseModel):
    __root__: List[Object]


app = FastAPI(title="Letters Sorting for license plate recognition [API]")


@app.post("/LetterSorting/", response_model=None,
          summary="To perform alphabets sorting",
          description="Hello world!")
async def letter_sorting(objects: Objects):
        x_coordinates = []
        labels = []
        scores = []
        if(objects):
            # print(type(objects))
            items = objects.__dict__
            # print(type(items['__root__']))
            for item in items['__root__']:
                locations = dict(dict(item)['location'])
                label = dict(item)['label']
                score = dict(item)['score']
                x_coordinates.append(locations['x'])
                labels.append(label)
                scores.append(score)
                # print(labels)

        # print(x_coordinates)
        #Convert to Numpy
        labels = np.array(labels)
        scores = np.average(scores)

        sorted_x = np.argsort(x_coordinates)
        sorted_labels = labels[sorted_x]
        isProvince = max(enumerate(sorted_labels), key=lambda x: len(x[1]))
        # print(isProvince, "lenght = ", len(isProvince[1]))
        
        #FOund Province then Check string length
        if len(isProvince[1]) > 1:
            plateNumber = sorted_labels.tolist()
            plateNumber.remove(isProvince[1])
            province = isProvince[1]
        else:
            plateNumber = sorted_labels.tolist()
            province = "Not found"

        
        plate_list = {
                        "plateNumber": ''.join(plateNumber),
                        "province": province,
                        "score": scores
                    }
            
        return plate_list


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=7200)